#include "ShowFunctionsWithOpenMPCalls.h"

#include "iostream"
#include "iomanip"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"

using namespace llvm;
using namespace std;

bool isOpenMPcall(const string I)
{
if ((I.compare(0,4,"omp_")==0)||(I.compare(0,7,"__kmpc_")==0)) return true;
return false;
}

void listfunctionswithOpenMPcalls(const Module *M)
{
  map<string,vector<string>> functionlist;
  for (const Function &F : *M) {
     vector<string> calllist;
     for (const BasicBlock &BB : F) {
        for (const Instruction &I : BB) {
           if (const CallInst *CI=dyn_cast<CallInst>(&I)) {
             if (isOpenMPcall(CI->getCalledFunction()->getName().str())) calllist.push_back(CI->getCalledFunction()->getName().str());
           }
        }
     }
     if (!calllist.empty()) functionlist[F.getName().str()]=calllist;
  }

// Mostrar listado por pantalla
  cout << "\n\n" << "LAB 33:" << "\n";
  if (!functionlist.empty()) {
    for (auto &fname : functionlist) {
       cout << fname.first << "\n";
       std::sort(fname.second.begin(),fname.second.end());
       fname.second.erase(std::unique(fname.second.begin(),fname.second.end()),fname.second.end());
       for (auto const &cname : fname.second) {
          cout << " " << cname << "\n";
       }
    }
  } else { cout << "No functions with OpenMP Calls." << "\n"; }
}
