; ModuleID = 'qla_bench.c'
source_filename = "qla_bench.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.timeval = type { i64, i64 }
%struct.timezone = type { i32, i32 }
%struct.QLA_F_Complex = type { float, float }

@.str = private unnamed_addr constant [20 x i8] c"QLA_Precision = %c\0A\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c"len = %i\0A\00", align 1
@.str.2 = private unnamed_addr constant [17 x i8] c"len/thread = %i\0A\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"%-32s:\00", align 1
@.str.4 = private unnamed_addr constant [22 x i8] c"QLA_V_vpeq_M_times_pV\00", align 1
@.str.5 = private unnamed_addr constant [40 x i8] c"%12g time=%5.2f mem=%5.0f mflops=%5.0f\0A\00", align 1
@.str.6 = private unnamed_addr constant [21 x i8] c"QLA_V_veq_Ma_times_V\00", align 1
@.str.7 = private unnamed_addr constant [14 x i8] c"QLA_V_vmeq_pV\00", align 1
@.str.8 = private unnamed_addr constant [29 x i8] c"QLA_D_vpeq_spproj_M_times_pD\00", align 1
@.str.9 = private unnamed_addr constant [21 x i8] c"QLA_M_veq_M_times_pM\00", align 1
@.str.10 = private unnamed_addr constant [18 x i8] c"QLA_r_veq_norm2_V\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"QLA_c_veq_V_dot_V\00", align 1

; Function Attrs: nounwind uwtable
define i8* @aligned_malloc(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = add i64 %5, 64
  %7 = call noalias i8* @malloc(i64 %6) #3
  %8 = ptrtoint i8* %7 to i64
  store i64 %8, i64* %3, align 8
  %9 = load i64, i64* %3, align 8
  %10 = urem i64 %9, 64
  store i64 %10, i64* %4, align 8
  %11 = load i64, i64* %4, align 8
  %12 = icmp ne i64 %11, 0
  br i1 %12, label %13, label %18

; <label>:13:                                     ; preds = %1
  %14 = load i64, i64* %4, align 8
  %15 = sub i64 64, %14
  %16 = load i64, i64* %3, align 8
  %17 = add i64 %16, %15
  store i64 %17, i64* %3, align 8
  br label %18

; <label>:18:                                     ; preds = %13, %1
  %19 = load i64, i64* %3, align 8
  %20 = inttoptr i64 %19 to i8*
  ret i8* %20
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #1

; Function Attrs: nounwind uwtable
define double @dtime() #0 {
  %1 = alloca %struct.timeval, align 8
  %2 = call i32 @gettimeofday(%struct.timeval* %1, %struct.timezone* null) #3
  %3 = getelementptr inbounds %struct.timeval, %struct.timeval* %1, i32 0, i32 0
  %4 = load i64, i64* %3, align 8
  %5 = sitofp i64 %4 to double
  %6 = getelementptr inbounds %struct.timeval, %struct.timeval* %1, i32 0, i32 1
  %7 = load i64, i64* %6, align 8
  %8 = sitofp i64 %7 to double
  %9 = fmul double 1.000000e-06, %8
  %10 = fadd double %5, %9
  %11 = fmul double 1.000000e+06, %10
  ret double %11
}

; Function Attrs: nounwind
declare i32 @gettimeofday(%struct.timeval*, %struct.timezone*) #1

; Function Attrs: nounwind uwtable
define void @set_R(float*, i32) #0 {
  %3 = alloca float*, align 8
  %4 = alloca i32, align 4
  store float* %0, float** %3, align 8
  store i32 %1, i32* %4, align 4
  %5 = load i32, i32* %4, align 4
  %6 = sitofp i32 %5 to double
  %7 = call double @cos(double %6) #3
  %8 = fadd double 1.000000e+00, %7
  %9 = fptrunc double %8 to float
  %10 = load float*, float** %3, align 8
  store float %9, float* %10, align 4
  ret void
}

; Function Attrs: nounwind
declare double @cos(double) #1

; Function Attrs: nounwind uwtable
define void @set_C(%struct.QLA_F_Complex*, i32) #0 {
  %3 = alloca %struct.QLA_F_Complex*, align 8
  %4 = alloca i32, align 4
  store %struct.QLA_F_Complex* %0, %struct.QLA_F_Complex** %3, align 8
  store i32 %1, i32* %4, align 4
  %5 = load i32, i32* %4, align 4
  %6 = sitofp i32 %5 to double
  %7 = call double @cos(double %6) #3
  %8 = fadd double 1.000000e+00, %7
  %9 = fptrunc double %8 to float
  %10 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %11 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %10, i32 0, i32 0
  store float %9, float* %11, align 8
  %12 = load i32, i32* %4, align 4
  %13 = sitofp i32 %12 to double
  %14 = call double @sin(double %13) #3
  %15 = fadd double 1.000000e+00, %14
  %16 = fptrunc double %15 to float
  %17 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %18 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %17, i32 0, i32 1
  store float %16, float* %18, align 4
  ret void
}

; Function Attrs: nounwind
declare double @sin(double) #1

; Function Attrs: nounwind uwtable
define void @set_V([3 x %struct.QLA_F_Complex]*, i32) #0 {
  %3 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store [3 x %struct.QLA_F_Complex]* %0, [3 x %struct.QLA_F_Complex]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %6

; <label>:6:                                      ; preds = %36, %2
  %7 = load i32, i32* %5, align 4
  %8 = icmp slt i32 %7, 3
  br i1 %8, label %9, label %39

; <label>:9:                                      ; preds = %6
  %10 = load i32, i32* %5, align 4
  %11 = add nsw i32 %10, 1
  %12 = sitofp i32 %11 to double
  %13 = load i32, i32* %4, align 4
  %14 = sitofp i32 %13 to double
  %15 = call double @cos(double %14) #3
  %16 = fadd double %12, %15
  %17 = fptrunc double %16 to float
  %18 = load i32, i32* %5, align 4
  %19 = sext i32 %18 to i64
  %20 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %21 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %20, i64 0, i64 %19
  %22 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %21, i32 0, i32 0
  store float %17, float* %22, align 8
  %23 = load i32, i32* %5, align 4
  %24 = add nsw i32 %23, 1
  %25 = sitofp i32 %24 to double
  %26 = load i32, i32* %4, align 4
  %27 = sitofp i32 %26 to double
  %28 = call double @sin(double %27) #3
  %29 = fadd double %25, %28
  %30 = fptrunc double %29 to float
  %31 = load i32, i32* %5, align 4
  %32 = sext i32 %31 to i64
  %33 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %34 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %33, i64 0, i64 %32
  %35 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %34, i32 0, i32 1
  store float %30, float* %35, align 4
  br label %36

; <label>:36:                                     ; preds = %9
  %37 = load i32, i32* %5, align 4
  %38 = add nsw i32 %37, 1
  store i32 %38, i32* %5, align 4
  br label %6

; <label>:39:                                     ; preds = %6
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_H([3 x [2 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [2 x %struct.QLA_F_Complex]]* %0, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %57, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %60

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %53, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 2
  br i1 %13, label %14, label %56

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = add nsw i32 %15, 4
  %17 = load i32, i32* %6, align 4
  %18 = add nsw i32 %17, 1
  %19 = mul nsw i32 %16, %18
  %20 = sitofp i32 %19 to double
  %21 = load i32, i32* %4, align 4
  %22 = sitofp i32 %21 to double
  %23 = call double @cos(double %22) #3
  %24 = fadd double %20, %23
  %25 = fptrunc double %24 to float
  %26 = load i32, i32* %6, align 4
  %27 = sext i32 %26 to i64
  %28 = load i32, i32* %5, align 4
  %29 = sext i32 %28 to i64
  %30 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %31 = getelementptr inbounds [3 x [2 x %struct.QLA_F_Complex]], [3 x [2 x %struct.QLA_F_Complex]]* %30, i64 0, i64 %29
  %32 = getelementptr inbounds [2 x %struct.QLA_F_Complex], [2 x %struct.QLA_F_Complex]* %31, i64 0, i64 %27
  %33 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %32, i32 0, i32 0
  store float %25, float* %33, align 8
  %34 = load i32, i32* %5, align 4
  %35 = add nsw i32 %34, 4
  %36 = load i32, i32* %6, align 4
  %37 = add nsw i32 %36, 1
  %38 = mul nsw i32 %35, %37
  %39 = sitofp i32 %38 to double
  %40 = load i32, i32* %4, align 4
  %41 = sitofp i32 %40 to double
  %42 = call double @sin(double %41) #3
  %43 = fadd double %39, %42
  %44 = fptrunc double %43 to float
  %45 = load i32, i32* %6, align 4
  %46 = sext i32 %45 to i64
  %47 = load i32, i32* %5, align 4
  %48 = sext i32 %47 to i64
  %49 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %50 = getelementptr inbounds [3 x [2 x %struct.QLA_F_Complex]], [3 x [2 x %struct.QLA_F_Complex]]* %49, i64 0, i64 %48
  %51 = getelementptr inbounds [2 x %struct.QLA_F_Complex], [2 x %struct.QLA_F_Complex]* %50, i64 0, i64 %46
  %52 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %51, i32 0, i32 1
  store float %44, float* %52, align 4
  br label %53

; <label>:53:                                     ; preds = %14
  %54 = load i32, i32* %6, align 4
  %55 = add nsw i32 %54, 1
  store i32 %55, i32* %6, align 4
  br label %11

; <label>:56:                                     ; preds = %11
  br label %57

; <label>:57:                                     ; preds = %56
  %58 = load i32, i32* %5, align 4
  %59 = add nsw i32 %58, 1
  store i32 %59, i32* %5, align 4
  br label %7

; <label>:60:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_D([3 x [4 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [4 x %struct.QLA_F_Complex]]* %0, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %57, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %60

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %53, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 4
  br i1 %13, label %14, label %56

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = add nsw i32 %15, 4
  %17 = load i32, i32* %6, align 4
  %18 = add nsw i32 %17, 1
  %19 = mul nsw i32 %16, %18
  %20 = sitofp i32 %19 to double
  %21 = load i32, i32* %4, align 4
  %22 = sitofp i32 %21 to double
  %23 = call double @cos(double %22) #3
  %24 = fadd double %20, %23
  %25 = fptrunc double %24 to float
  %26 = load i32, i32* %6, align 4
  %27 = sext i32 %26 to i64
  %28 = load i32, i32* %5, align 4
  %29 = sext i32 %28 to i64
  %30 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %31 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %30, i64 0, i64 %29
  %32 = getelementptr inbounds [4 x %struct.QLA_F_Complex], [4 x %struct.QLA_F_Complex]* %31, i64 0, i64 %27
  %33 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %32, i32 0, i32 0
  store float %25, float* %33, align 8
  %34 = load i32, i32* %5, align 4
  %35 = add nsw i32 %34, 4
  %36 = load i32, i32* %6, align 4
  %37 = add nsw i32 %36, 1
  %38 = mul nsw i32 %35, %37
  %39 = sitofp i32 %38 to double
  %40 = load i32, i32* %4, align 4
  %41 = sitofp i32 %40 to double
  %42 = call double @sin(double %41) #3
  %43 = fadd double %39, %42
  %44 = fptrunc double %43 to float
  %45 = load i32, i32* %6, align 4
  %46 = sext i32 %45 to i64
  %47 = load i32, i32* %5, align 4
  %48 = sext i32 %47 to i64
  %49 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %50 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %49, i64 0, i64 %48
  %51 = getelementptr inbounds [4 x %struct.QLA_F_Complex], [4 x %struct.QLA_F_Complex]* %50, i64 0, i64 %46
  %52 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %51, i32 0, i32 1
  store float %44, float* %52, align 4
  br label %53

; <label>:53:                                     ; preds = %14
  %54 = load i32, i32* %6, align 4
  %55 = add nsw i32 %54, 1
  store i32 %55, i32* %6, align 4
  br label %11

; <label>:56:                                     ; preds = %11
  br label %57

; <label>:57:                                     ; preds = %56
  %58 = load i32, i32* %5, align 4
  %59 = add nsw i32 %58, 1
  store i32 %59, i32* %5, align 4
  br label %7

; <label>:60:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define void @set_M([3 x [3 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store [3 x [3 x %struct.QLA_F_Complex]]* %0, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store i32 0, i32* %5, align 4
  br label %7

; <label>:7:                                      ; preds = %64, %2
  %8 = load i32, i32* %5, align 4
  %9 = icmp slt i32 %8, 3
  br i1 %9, label %10, label %67

; <label>:10:                                     ; preds = %7
  store i32 0, i32* %6, align 4
  br label %11

; <label>:11:                                     ; preds = %60, %10
  %12 = load i32, i32* %6, align 4
  %13 = icmp slt i32 %12, 3
  br i1 %13, label %14, label %63

; <label>:14:                                     ; preds = %11
  %15 = load i32, i32* %5, align 4
  %16 = load i32, i32* %6, align 4
  %17 = sub nsw i32 %15, %16
  %18 = add nsw i32 %17, 3
  %19 = add nsw i32 %18, 1
  %20 = load i32, i32* %5, align 4
  %21 = load i32, i32* %6, align 4
  %22 = add nsw i32 %20, %21
  %23 = add nsw i32 %22, 1
  %24 = mul nsw i32 %19, %23
  %25 = srem i32 %24, 19
  %26 = sitofp i32 %25 to double
  %27 = load i32, i32* %4, align 4
  %28 = sitofp i32 %27 to double
  %29 = call double @cos(double %28) #3
  %30 = fadd double %26, %29
  %31 = fptrunc double %30 to float
  %32 = load i32, i32* %6, align 4
  %33 = sext i32 %32 to i64
  %34 = load i32, i32* %5, align 4
  %35 = sext i32 %34 to i64
  %36 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %37 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %36, i64 0, i64 %35
  %38 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %37, i64 0, i64 %33
  %39 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %38, i32 0, i32 0
  store float %31, float* %39, align 8
  %40 = load i32, i32* %5, align 4
  %41 = add nsw i32 %40, 4
  %42 = load i32, i32* %6, align 4
  %43 = add nsw i32 %42, 1
  %44 = mul nsw i32 %41, %43
  %45 = srem i32 %44, 17
  %46 = sitofp i32 %45 to double
  %47 = load i32, i32* %4, align 4
  %48 = sitofp i32 %47 to double
  %49 = call double @sin(double %48) #3
  %50 = fadd double %46, %49
  %51 = fptrunc double %50 to float
  %52 = load i32, i32* %6, align 4
  %53 = sext i32 %52 to i64
  %54 = load i32, i32* %5, align 4
  %55 = sext i32 %54 to i64
  %56 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %57 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %56, i64 0, i64 %55
  %58 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %57, i64 0, i64 %53
  %59 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %58, i32 0, i32 1
  store float %51, float* %59, align 4
  br label %60

; <label>:60:                                     ; preds = %14
  %61 = load i32, i32* %6, align 4
  %62 = add nsw i32 %61, 1
  store i32 %62, i32* %6, align 4
  br label %11

; <label>:63:                                     ; preds = %11
  br label %64

; <label>:64:                                     ; preds = %63
  %65 = load i32, i32* %5, align 4
  %66 = add nsw i32 %65, 1
  store i32 %66, i32* %5, align 4
  br label %7

; <label>:67:                                     ; preds = %7
  ret void
}

; Function Attrs: nounwind uwtable
define float @sum_C(%struct.QLA_F_Complex*, i32) #0 {
  %3 = alloca %struct.QLA_F_Complex*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store %struct.QLA_F_Complex* %0, %struct.QLA_F_Complex** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %3, align 8
  %10 = bitcast %struct.QLA_F_Complex* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 8
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_V([3 x %struct.QLA_F_Complex]*, i32) #0 {
  %3 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x %struct.QLA_F_Complex]* %0, [3 x %struct.QLA_F_Complex]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %3, align 8
  %10 = bitcast [3 x %struct.QLA_F_Complex]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 24
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_H([3 x [2 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [2 x %struct.QLA_F_Complex]]* %0, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [2 x %struct.QLA_F_Complex]]*, [3 x [2 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [2 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 48
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_D([3 x [4 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [4 x %struct.QLA_F_Complex]]* %0, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [4 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 96
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define float @sum_M([3 x [3 x %struct.QLA_F_Complex]]*, i32) #0 {
  %3 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %4 = alloca i32, align 4
  %5 = alloca float, align 4
  %6 = alloca float*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store [3 x [3 x %struct.QLA_F_Complex]]* %0, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  store i32 %1, i32* %4, align 4
  store float 0.000000e+00, float* %5, align 4
  %9 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %3, align 8
  %10 = bitcast [3 x [3 x %struct.QLA_F_Complex]]* %9 to float*
  store float* %10, float** %6, align 8
  %11 = load i32, i32* %4, align 4
  %12 = sext i32 %11 to i64
  %13 = mul i64 %12, 72
  %14 = udiv i64 %13, 4
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %7, align 4
  store i32 0, i32* %8, align 4
  br label %16

; <label>:16:                                     ; preds = %28, %2
  %17 = load i32, i32* %8, align 4
  %18 = load i32, i32* %7, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %16
  %21 = load i32, i32* %8, align 4
  %22 = sext i32 %21 to i64
  %23 = load float*, float** %6, align 8
  %24 = getelementptr inbounds float, float* %23, i64 %22
  %25 = load float, float* %24, align 4
  %26 = load float, float* %5, align 4
  %27 = fadd float %26, %25
  store float %27, float* %5, align 4
  br label %28

; <label>:28:                                     ; preds = %20
  %29 = load i32, i32* %8, align 4
  %30 = add nsw i32 %29, 1
  store i32 %30, i32* %8, align 4
  br label %16

; <label>:31:                                     ; preds = %16
  %32 = load float, float* %5, align 4
  %33 = load i32, i32* %7, align 4
  %34 = sitofp i32 %33 to float
  %35 = fdiv float %32, %34
  ret float %35
}

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca float, align 4
  %7 = alloca float*, align 8
  %8 = alloca %struct.QLA_F_Complex*, align 8
  %9 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %10 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %11 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %12 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %13 = alloca [3 x %struct.QLA_F_Complex]*, align 8
  %14 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %15 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %16 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %17 = alloca [3 x %struct.QLA_F_Complex]**, align 8
  %18 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %19 = alloca [3 x [2 x %struct.QLA_F_Complex]]*, align 8
  %20 = alloca [3 x [2 x %struct.QLA_F_Complex]]**, align 8
  %21 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %22 = alloca [3 x [4 x %struct.QLA_F_Complex]]*, align 8
  %23 = alloca [3 x [4 x %struct.QLA_F_Complex]]**, align 8
  %24 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %25 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %26 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %27 = alloca [3 x [3 x %struct.QLA_F_Complex]]*, align 8
  %28 = alloca [3 x [3 x %struct.QLA_F_Complex]]**, align 8
  %29 = alloca double, align 8
  %30 = alloca double, align 8
  %31 = alloca double, align 8
  %32 = alloca i32, align 4
  %33 = alloca i32, align 4
  %34 = alloca i32, align 4
  %35 = alloca i32, align 4
  %36 = alloca i32, align 4
  %37 = alloca double, align 8
  %38 = alloca i32, align 4
  %39 = alloca i32, align 4
  %40 = alloca i32, align 4
  %41 = alloca i32, align 4
  %42 = alloca i32, align 4
  %43 = alloca i32, align 4
  %44 = alloca i32, align 4
  %45 = alloca i32, align 4
  %46 = alloca i32, align 4
  %47 = alloca i32, align 4
  %48 = alloca i32, align 4
  %49 = alloca i32, align 4
  %50 = alloca i32, align 4
  %51 = alloca i32, align 4
  %52 = alloca i32, align 4
  %53 = alloca i32, align 4
  %54 = alloca i32, align 4
  %55 = alloca i32, align 4
  %56 = alloca i32, align 4
  %57 = alloca i32, align 4
  %58 = alloca i32, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i32 1, i32* %35, align 4
  %59 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i32 70)
  %60 = load i32, i32* %35, align 4
  %61 = mul nsw i32 64, %60
  store i32 %61, i32* %32, align 4
  %62 = load i32, i32* %35, align 4
  %63 = mul nsw i32 262144, %62
  store i32 %63, i32* %33, align 4
  %64 = load i32, i32* %33, align 4
  %65 = sext i32 %64 to i64
  %66 = mul i64 %65, 4
  %67 = call i8* @aligned_malloc(i64 %66)
  %68 = bitcast i8* %67 to float*
  store float* %68, float** %7, align 8
  %69 = load i32, i32* %33, align 4
  %70 = sext i32 %69 to i64
  %71 = mul i64 %70, 8
  %72 = call i8* @aligned_malloc(i64 %71)
  %73 = bitcast i8* %72 to %struct.QLA_F_Complex*
  store %struct.QLA_F_Complex* %73, %struct.QLA_F_Complex** %8, align 8
  %74 = load i32, i32* %33, align 4
  %75 = sext i32 %74 to i64
  %76 = mul i64 %75, 24
  %77 = call i8* @aligned_malloc(i64 %76)
  %78 = bitcast i8* %77 to [3 x %struct.QLA_F_Complex]*
  store [3 x %struct.QLA_F_Complex]* %78, [3 x %struct.QLA_F_Complex]** %9, align 8
  %79 = load i32, i32* %33, align 4
  %80 = sext i32 %79 to i64
  %81 = mul i64 %80, 24
  %82 = call i8* @aligned_malloc(i64 %81)
  %83 = bitcast i8* %82 to [3 x %struct.QLA_F_Complex]*
  store [3 x %struct.QLA_F_Complex]* %83, [3 x %struct.QLA_F_Complex]** %10, align 8
  %84 = load i32, i32* %33, align 4
  %85 = sext i32 %84 to i64
  %86 = mul i64 %85, 8
  %87 = call i8* @aligned_malloc(i64 %86)
  %88 = bitcast i8* %87 to [3 x %struct.QLA_F_Complex]**
  store [3 x %struct.QLA_F_Complex]** %88, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %89 = load i32, i32* %33, align 4
  %90 = sext i32 %89 to i64
  %91 = mul i64 %90, 96
  %92 = call i8* @aligned_malloc(i64 %91)
  %93 = bitcast i8* %92 to [3 x [4 x %struct.QLA_F_Complex]]*
  store [3 x [4 x %struct.QLA_F_Complex]]* %93, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %94 = load i32, i32* %33, align 4
  %95 = sext i32 %94 to i64
  %96 = mul i64 %95, 96
  %97 = call i8* @aligned_malloc(i64 %96)
  %98 = bitcast i8* %97 to [3 x [4 x %struct.QLA_F_Complex]]*
  store [3 x [4 x %struct.QLA_F_Complex]]* %98, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %99 = load i32, i32* %33, align 4
  %100 = sext i32 %99 to i64
  %101 = mul i64 %100, 8
  %102 = call i8* @aligned_malloc(i64 %101)
  %103 = bitcast i8* %102 to [3 x [4 x %struct.QLA_F_Complex]]**
  store [3 x [4 x %struct.QLA_F_Complex]]** %103, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %104 = load i32, i32* %33, align 4
  %105 = sext i32 %104 to i64
  %106 = mul i64 %105, 72
  %107 = call i8* @aligned_malloc(i64 %106)
  %108 = bitcast i8* %107 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %108, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %109 = load i32, i32* %33, align 4
  %110 = sext i32 %109 to i64
  %111 = mul i64 %110, 72
  %112 = call i8* @aligned_malloc(i64 %111)
  %113 = bitcast i8* %112 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %113, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %114 = load i32, i32* %33, align 4
  %115 = sext i32 %114 to i64
  %116 = mul i64 %115, 72
  %117 = call i8* @aligned_malloc(i64 %116)
  %118 = bitcast i8* %117 to [3 x [3 x %struct.QLA_F_Complex]]*
  store [3 x [3 x %struct.QLA_F_Complex]]* %118, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %119 = load i32, i32* %33, align 4
  %120 = sext i32 %119 to i64
  %121 = mul i64 %120, 8
  %122 = call i8* @aligned_malloc(i64 %121)
  %123 = bitcast i8* %122 to [3 x [3 x %struct.QLA_F_Complex]]**
  store [3 x [3 x %struct.QLA_F_Complex]]** %123, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %124 = load i32, i32* %32, align 4
  store i32 %124, i32* %36, align 4
  br label %125

; <label>:125:                                    ; preds = %1093, %2
  %126 = load i32, i32* %36, align 4
  %127 = load i32, i32* %33, align 4
  %128 = icmp sle i32 %126, %127
  br i1 %128, label %129, label %1096

; <label>:129:                                    ; preds = %125
  %130 = load i32, i32* %36, align 4
  %131 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), i32 %130)
  %132 = load i32, i32* %36, align 4
  %133 = load i32, i32* %35, align 4
  %134 = sdiv i32 %132, %133
  %135 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.2, i32 0, i32 0), i32 %134)
  %136 = load i32, i32* %36, align 4
  %137 = sitofp i32 %136 to double
  %138 = fdiv double 9.000000e+09, %137
  store double %138, double* %37, align 8
  store i32 0, i32* %38, align 4
  br label %139

; <label>:139:                                    ; preds = %218, %129
  %140 = load i32, i32* %38, align 4
  %141 = load i32, i32* %36, align 4
  %142 = icmp slt i32 %140, %141
  br i1 %142, label %143, label %221

; <label>:143:                                    ; preds = %139
  %144 = load i32, i32* %38, align 4
  %145 = sext i32 %144 to i64
  %146 = load float*, float** %7, align 8
  %147 = getelementptr inbounds float, float* %146, i64 %145
  %148 = load i32, i32* %38, align 4
  call void @set_R(float* %147, i32 %148)
  %149 = load i32, i32* %38, align 4
  %150 = sext i32 %149 to i64
  %151 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %152 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %151, i64 %150
  %153 = load i32, i32* %38, align 4
  call void @set_C(%struct.QLA_F_Complex* %152, i32 %153)
  %154 = load i32, i32* %38, align 4
  %155 = sext i32 %154 to i64
  %156 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %157 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %156, i64 %155
  %158 = load i32, i32* %38, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %157, i32 %158)
  %159 = load i32, i32* %38, align 4
  %160 = sext i32 %159 to i64
  %161 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %162 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %161, i64 %160
  %163 = load i32, i32* %38, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %162, i32 %163)
  %164 = load i32, i32* %38, align 4
  %165 = sext i32 %164 to i64
  %166 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %167 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %166, i64 %165
  %168 = load i32, i32* %38, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %167, i32 %168)
  %169 = load i32, i32* %38, align 4
  %170 = sext i32 %169 to i64
  %171 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %172 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %171, i64 %170
  %173 = load i32, i32* %38, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %172, i32 %173)
  %174 = load i32, i32* %38, align 4
  %175 = sext i32 %174 to i64
  %176 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %177 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %176, i64 %175
  %178 = load i32, i32* %38, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %177, i32 %178)
  %179 = load i32, i32* %38, align 4
  %180 = sext i32 %179 to i64
  %181 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %182 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %181, i64 %180
  %183 = load i32, i32* %38, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %182, i32 %183)
  %184 = load i32, i32* %38, align 4
  %185 = sext i32 %184 to i64
  %186 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %187 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %186, i64 %185
  %188 = load i32, i32* %38, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %187, i32 %188)
  %189 = load i32, i32* %38, align 4
  %190 = or i32 %189, 16
  %191 = add nsw i32 %190, 256
  %192 = load i32, i32* %36, align 4
  %193 = srem i32 %191, %192
  store i32 %193, i32* %39, align 4
  %194 = load i32, i32* %39, align 4
  %195 = sext i32 %194 to i64
  %196 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %197 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %196, i64 %195
  %198 = load i32, i32* %38, align 4
  %199 = sext i32 %198 to i64
  %200 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %201 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %200, i64 %199
  store [3 x %struct.QLA_F_Complex]* %197, [3 x %struct.QLA_F_Complex]** %201, align 8
  %202 = load i32, i32* %39, align 4
  %203 = sext i32 %202 to i64
  %204 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %205 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %204, i64 %203
  %206 = load i32, i32* %38, align 4
  %207 = sext i32 %206 to i64
  %208 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %209 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %208, i64 %207
  store [3 x [4 x %struct.QLA_F_Complex]]* %205, [3 x [4 x %struct.QLA_F_Complex]]** %209, align 8
  %210 = load i32, i32* %39, align 4
  %211 = sext i32 %210 to i64
  %212 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %213 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %212, i64 %211
  %214 = load i32, i32* %38, align 4
  %215 = sext i32 %214 to i64
  %216 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %217 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %216, i64 %215
  store [3 x [3 x %struct.QLA_F_Complex]]* %213, [3 x [3 x %struct.QLA_F_Complex]]** %217, align 8
  br label %218

; <label>:218:                                    ; preds = %143
  %219 = load i32, i32* %38, align 4
  %220 = add nsw i32 %219, 1
  store i32 %220, i32* %38, align 4
  br label %139

; <label>:221:                                    ; preds = %139
  store double 1.440000e+02, double* %30, align 8
  store double 7.200000e+01, double* %29, align 8
  %222 = load double, double* %37, align 8
  %223 = load double, double* %29, align 8
  %224 = load double, double* %30, align 8
  %225 = fadd double %223, %224
  %226 = fdiv double %222, %225
  %227 = fadd double 1.000000e+00, %226
  %228 = fptosi double %227 to i32
  store i32 %228, i32* %34, align 4
  %229 = call double @dtime()
  store double %229, double* %31, align 8
  store i32 0, i32* %40, align 4
  br label %230

; <label>:230:                                    ; preds = %239, %221
  %231 = load i32, i32* %40, align 4
  %232 = load i32, i32* %34, align 4
  %233 = icmp slt i32 %231, %232
  br i1 %233, label %234, label %242

; <label>:234:                                    ; preds = %230
  %235 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %236 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %237 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %238 = load i32, i32* %36, align 4
  call void @QLA_F3_V_vpeq_M_times_pV([3 x %struct.QLA_F_Complex]* %235, [3 x [3 x %struct.QLA_F_Complex]]* %236, [3 x %struct.QLA_F_Complex]** %237, i32 %238)
  br label %239

; <label>:239:                                    ; preds = %234
  %240 = load i32, i32* %40, align 4
  %241 = add nsw i32 %240, 1
  store i32 %241, i32* %40, align 4
  br label %230

; <label>:242:                                    ; preds = %230
  %243 = call double @dtime()
  %244 = load double, double* %31, align 8
  %245 = fsub double %243, %244
  store double %245, double* %31, align 8
  %246 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %247 = load i32, i32* %36, align 4
  %248 = call float @sum_V([3 x %struct.QLA_F_Complex]* %246, i32 %247)
  store float %248, float* %6, align 4
  %249 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0))
  %250 = load float, float* %6, align 4
  %251 = fpext float %250 to double
  %252 = load double, double* %31, align 8
  %253 = load double, double* %30, align 8
  %254 = load i32, i32* %36, align 4
  %255 = sitofp i32 %254 to double
  %256 = fmul double %253, %255
  %257 = load i32, i32* %34, align 4
  %258 = sitofp i32 %257 to double
  %259 = fmul double %256, %258
  %260 = load double, double* %31, align 8
  %261 = fmul double 1.000000e+06, %260
  %262 = fdiv double %259, %261
  %263 = load double, double* %29, align 8
  %264 = load i32, i32* %36, align 4
  %265 = sitofp i32 %264 to double
  %266 = fmul double %263, %265
  %267 = load i32, i32* %34, align 4
  %268 = sitofp i32 %267 to double
  %269 = fmul double %266, %268
  %270 = load double, double* %31, align 8
  %271 = fmul double 1.000000e+06, %270
  %272 = fdiv double %269, %271
  %273 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %251, double %252, double %262, double %272)
  store i32 0, i32* %41, align 4
  br label %274

; <label>:274:                                    ; preds = %353, %242
  %275 = load i32, i32* %41, align 4
  %276 = load i32, i32* %36, align 4
  %277 = icmp slt i32 %275, %276
  br i1 %277, label %278, label %356

; <label>:278:                                    ; preds = %274
  %279 = load i32, i32* %41, align 4
  %280 = sext i32 %279 to i64
  %281 = load float*, float** %7, align 8
  %282 = getelementptr inbounds float, float* %281, i64 %280
  %283 = load i32, i32* %41, align 4
  call void @set_R(float* %282, i32 %283)
  %284 = load i32, i32* %41, align 4
  %285 = sext i32 %284 to i64
  %286 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %287 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %286, i64 %285
  %288 = load i32, i32* %41, align 4
  call void @set_C(%struct.QLA_F_Complex* %287, i32 %288)
  %289 = load i32, i32* %41, align 4
  %290 = sext i32 %289 to i64
  %291 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %292 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %291, i64 %290
  %293 = load i32, i32* %41, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %292, i32 %293)
  %294 = load i32, i32* %41, align 4
  %295 = sext i32 %294 to i64
  %296 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %297 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %296, i64 %295
  %298 = load i32, i32* %41, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %297, i32 %298)
  %299 = load i32, i32* %41, align 4
  %300 = sext i32 %299 to i64
  %301 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %302 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %301, i64 %300
  %303 = load i32, i32* %41, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %302, i32 %303)
  %304 = load i32, i32* %41, align 4
  %305 = sext i32 %304 to i64
  %306 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %307 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %306, i64 %305
  %308 = load i32, i32* %41, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %307, i32 %308)
  %309 = load i32, i32* %41, align 4
  %310 = sext i32 %309 to i64
  %311 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %312 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %311, i64 %310
  %313 = load i32, i32* %41, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %312, i32 %313)
  %314 = load i32, i32* %41, align 4
  %315 = sext i32 %314 to i64
  %316 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %317 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %316, i64 %315
  %318 = load i32, i32* %41, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %317, i32 %318)
  %319 = load i32, i32* %41, align 4
  %320 = sext i32 %319 to i64
  %321 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %322 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %321, i64 %320
  %323 = load i32, i32* %41, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %322, i32 %323)
  %324 = load i32, i32* %41, align 4
  %325 = or i32 %324, 16
  %326 = add nsw i32 %325, 256
  %327 = load i32, i32* %36, align 4
  %328 = srem i32 %326, %327
  store i32 %328, i32* %42, align 4
  %329 = load i32, i32* %42, align 4
  %330 = sext i32 %329 to i64
  %331 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %332 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %331, i64 %330
  %333 = load i32, i32* %41, align 4
  %334 = sext i32 %333 to i64
  %335 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %336 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %335, i64 %334
  store [3 x %struct.QLA_F_Complex]* %332, [3 x %struct.QLA_F_Complex]** %336, align 8
  %337 = load i32, i32* %42, align 4
  %338 = sext i32 %337 to i64
  %339 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %340 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %339, i64 %338
  %341 = load i32, i32* %41, align 4
  %342 = sext i32 %341 to i64
  %343 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %344 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %343, i64 %342
  store [3 x [4 x %struct.QLA_F_Complex]]* %340, [3 x [4 x %struct.QLA_F_Complex]]** %344, align 8
  %345 = load i32, i32* %42, align 4
  %346 = sext i32 %345 to i64
  %347 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %348 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %347, i64 %346
  %349 = load i32, i32* %41, align 4
  %350 = sext i32 %349 to i64
  %351 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %352 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %351, i64 %350
  store [3 x [3 x %struct.QLA_F_Complex]]* %348, [3 x [3 x %struct.QLA_F_Complex]]** %352, align 8
  br label %353

; <label>:353:                                    ; preds = %278
  %354 = load i32, i32* %41, align 4
  %355 = add nsw i32 %354, 1
  store i32 %355, i32* %41, align 4
  br label %274

; <label>:356:                                    ; preds = %274
  store double 1.200000e+02, double* %30, align 8
  store double 6.600000e+01, double* %29, align 8
  %357 = load double, double* %37, align 8
  %358 = load double, double* %29, align 8
  %359 = load double, double* %30, align 8
  %360 = fadd double %358, %359
  %361 = fdiv double %357, %360
  %362 = fadd double 1.000000e+00, %361
  %363 = fptosi double %362 to i32
  store i32 %363, i32* %34, align 4
  %364 = call double @dtime()
  store double %364, double* %31, align 8
  store i32 0, i32* %43, align 4
  br label %365

; <label>:365:                                    ; preds = %374, %356
  %366 = load i32, i32* %43, align 4
  %367 = load i32, i32* %34, align 4
  %368 = icmp slt i32 %366, %367
  br i1 %368, label %369, label %377

; <label>:369:                                    ; preds = %365
  %370 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %371 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %372 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %373 = load i32, i32* %36, align 4
  call void @QLA_F3_V_veq_Ma_times_V([3 x %struct.QLA_F_Complex]* %370, [3 x [3 x %struct.QLA_F_Complex]]* %371, [3 x %struct.QLA_F_Complex]* %372, i32 %373)
  br label %374

; <label>:374:                                    ; preds = %369
  %375 = load i32, i32* %43, align 4
  %376 = add nsw i32 %375, 1
  store i32 %376, i32* %43, align 4
  br label %365

; <label>:377:                                    ; preds = %365
  %378 = call double @dtime()
  %379 = load double, double* %31, align 8
  %380 = fsub double %378, %379
  store double %380, double* %31, align 8
  %381 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %382 = load i32, i32* %36, align 4
  %383 = call float @sum_V([3 x %struct.QLA_F_Complex]* %381, i32 %382)
  store float %383, float* %6, align 4
  %384 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.6, i32 0, i32 0))
  %385 = load float, float* %6, align 4
  %386 = fpext float %385 to double
  %387 = load double, double* %31, align 8
  %388 = load double, double* %30, align 8
  %389 = load i32, i32* %36, align 4
  %390 = sitofp i32 %389 to double
  %391 = fmul double %388, %390
  %392 = load i32, i32* %34, align 4
  %393 = sitofp i32 %392 to double
  %394 = fmul double %391, %393
  %395 = load double, double* %31, align 8
  %396 = fmul double 1.000000e+06, %395
  %397 = fdiv double %394, %396
  %398 = load double, double* %29, align 8
  %399 = load i32, i32* %36, align 4
  %400 = sitofp i32 %399 to double
  %401 = fmul double %398, %400
  %402 = load i32, i32* %34, align 4
  %403 = sitofp i32 %402 to double
  %404 = fmul double %401, %403
  %405 = load double, double* %31, align 8
  %406 = fmul double 1.000000e+06, %405
  %407 = fdiv double %404, %406
  %408 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %386, double %387, double %397, double %407)
  store i32 0, i32* %44, align 4
  br label %409

; <label>:409:                                    ; preds = %488, %377
  %410 = load i32, i32* %44, align 4
  %411 = load i32, i32* %36, align 4
  %412 = icmp slt i32 %410, %411
  br i1 %412, label %413, label %491

; <label>:413:                                    ; preds = %409
  %414 = load i32, i32* %44, align 4
  %415 = sext i32 %414 to i64
  %416 = load float*, float** %7, align 8
  %417 = getelementptr inbounds float, float* %416, i64 %415
  %418 = load i32, i32* %44, align 4
  call void @set_R(float* %417, i32 %418)
  %419 = load i32, i32* %44, align 4
  %420 = sext i32 %419 to i64
  %421 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %422 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %421, i64 %420
  %423 = load i32, i32* %44, align 4
  call void @set_C(%struct.QLA_F_Complex* %422, i32 %423)
  %424 = load i32, i32* %44, align 4
  %425 = sext i32 %424 to i64
  %426 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %427 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %426, i64 %425
  %428 = load i32, i32* %44, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %427, i32 %428)
  %429 = load i32, i32* %44, align 4
  %430 = sext i32 %429 to i64
  %431 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %432 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %431, i64 %430
  %433 = load i32, i32* %44, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %432, i32 %433)
  %434 = load i32, i32* %44, align 4
  %435 = sext i32 %434 to i64
  %436 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %437 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %436, i64 %435
  %438 = load i32, i32* %44, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %437, i32 %438)
  %439 = load i32, i32* %44, align 4
  %440 = sext i32 %439 to i64
  %441 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %442 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %441, i64 %440
  %443 = load i32, i32* %44, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %442, i32 %443)
  %444 = load i32, i32* %44, align 4
  %445 = sext i32 %444 to i64
  %446 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %447 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %446, i64 %445
  %448 = load i32, i32* %44, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %447, i32 %448)
  %449 = load i32, i32* %44, align 4
  %450 = sext i32 %449 to i64
  %451 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %452 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %451, i64 %450
  %453 = load i32, i32* %44, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %452, i32 %453)
  %454 = load i32, i32* %44, align 4
  %455 = sext i32 %454 to i64
  %456 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %457 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %456, i64 %455
  %458 = load i32, i32* %44, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %457, i32 %458)
  %459 = load i32, i32* %44, align 4
  %460 = or i32 %459, 16
  %461 = add nsw i32 %460, 256
  %462 = load i32, i32* %36, align 4
  %463 = srem i32 %461, %462
  store i32 %463, i32* %45, align 4
  %464 = load i32, i32* %45, align 4
  %465 = sext i32 %464 to i64
  %466 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %467 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %466, i64 %465
  %468 = load i32, i32* %44, align 4
  %469 = sext i32 %468 to i64
  %470 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %471 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %470, i64 %469
  store [3 x %struct.QLA_F_Complex]* %467, [3 x %struct.QLA_F_Complex]** %471, align 8
  %472 = load i32, i32* %45, align 4
  %473 = sext i32 %472 to i64
  %474 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %475 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %474, i64 %473
  %476 = load i32, i32* %44, align 4
  %477 = sext i32 %476 to i64
  %478 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %479 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %478, i64 %477
  store [3 x [4 x %struct.QLA_F_Complex]]* %475, [3 x [4 x %struct.QLA_F_Complex]]** %479, align 8
  %480 = load i32, i32* %45, align 4
  %481 = sext i32 %480 to i64
  %482 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %483 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %482, i64 %481
  %484 = load i32, i32* %44, align 4
  %485 = sext i32 %484 to i64
  %486 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %487 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %486, i64 %485
  store [3 x [3 x %struct.QLA_F_Complex]]* %483, [3 x [3 x %struct.QLA_F_Complex]]** %487, align 8
  br label %488

; <label>:488:                                    ; preds = %413
  %489 = load i32, i32* %44, align 4
  %490 = add nsw i32 %489, 1
  store i32 %490, i32* %44, align 4
  br label %409

; <label>:491:                                    ; preds = %409
  store double 7.200000e+01, double* %30, align 8
  store double 6.000000e+00, double* %29, align 8
  %492 = load double, double* %37, align 8
  %493 = load double, double* %29, align 8
  %494 = load double, double* %30, align 8
  %495 = fadd double %493, %494
  %496 = fdiv double %492, %495
  %497 = fadd double 1.000000e+00, %496
  %498 = fptosi double %497 to i32
  store i32 %498, i32* %34, align 4
  %499 = call double @dtime()
  store double %499, double* %31, align 8
  store i32 0, i32* %46, align 4
  br label %500

; <label>:500:                                    ; preds = %508, %491
  %501 = load i32, i32* %46, align 4
  %502 = load i32, i32* %34, align 4
  %503 = icmp slt i32 %501, %502
  br i1 %503, label %504, label %511

; <label>:504:                                    ; preds = %500
  %505 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %506 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %507 = load i32, i32* %36, align 4
  call void @QLA_F3_V_vmeq_pV([3 x %struct.QLA_F_Complex]* %505, [3 x %struct.QLA_F_Complex]** %506, i32 %507)
  br label %508

; <label>:508:                                    ; preds = %504
  %509 = load i32, i32* %46, align 4
  %510 = add nsw i32 %509, 1
  store i32 %510, i32* %46, align 4
  br label %500

; <label>:511:                                    ; preds = %500
  %512 = call double @dtime()
  %513 = load double, double* %31, align 8
  %514 = fsub double %512, %513
  store double %514, double* %31, align 8
  %515 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %516 = load i32, i32* %36, align 4
  %517 = call float @sum_V([3 x %struct.QLA_F_Complex]* %515, i32 %516)
  store float %517, float* %6, align 4
  %518 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.7, i32 0, i32 0))
  %519 = load float, float* %6, align 4
  %520 = fpext float %519 to double
  %521 = load double, double* %31, align 8
  %522 = load double, double* %30, align 8
  %523 = load i32, i32* %36, align 4
  %524 = sitofp i32 %523 to double
  %525 = fmul double %522, %524
  %526 = load i32, i32* %34, align 4
  %527 = sitofp i32 %526 to double
  %528 = fmul double %525, %527
  %529 = load double, double* %31, align 8
  %530 = fmul double 1.000000e+06, %529
  %531 = fdiv double %528, %530
  %532 = load double, double* %29, align 8
  %533 = load i32, i32* %36, align 4
  %534 = sitofp i32 %533 to double
  %535 = fmul double %532, %534
  %536 = load i32, i32* %34, align 4
  %537 = sitofp i32 %536 to double
  %538 = fmul double %535, %537
  %539 = load double, double* %31, align 8
  %540 = fmul double 1.000000e+06, %539
  %541 = fdiv double %538, %540
  %542 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %520, double %521, double %531, double %541)
  store i32 0, i32* %47, align 4
  br label %543

; <label>:543:                                    ; preds = %622, %511
  %544 = load i32, i32* %47, align 4
  %545 = load i32, i32* %36, align 4
  %546 = icmp slt i32 %544, %545
  br i1 %546, label %547, label %625

; <label>:547:                                    ; preds = %543
  %548 = load i32, i32* %47, align 4
  %549 = sext i32 %548 to i64
  %550 = load float*, float** %7, align 8
  %551 = getelementptr inbounds float, float* %550, i64 %549
  %552 = load i32, i32* %47, align 4
  call void @set_R(float* %551, i32 %552)
  %553 = load i32, i32* %47, align 4
  %554 = sext i32 %553 to i64
  %555 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %556 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %555, i64 %554
  %557 = load i32, i32* %47, align 4
  call void @set_C(%struct.QLA_F_Complex* %556, i32 %557)
  %558 = load i32, i32* %47, align 4
  %559 = sext i32 %558 to i64
  %560 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %561 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %560, i64 %559
  %562 = load i32, i32* %47, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %561, i32 %562)
  %563 = load i32, i32* %47, align 4
  %564 = sext i32 %563 to i64
  %565 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %566 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %565, i64 %564
  %567 = load i32, i32* %47, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %566, i32 %567)
  %568 = load i32, i32* %47, align 4
  %569 = sext i32 %568 to i64
  %570 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %571 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %570, i64 %569
  %572 = load i32, i32* %47, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %571, i32 %572)
  %573 = load i32, i32* %47, align 4
  %574 = sext i32 %573 to i64
  %575 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %576 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %575, i64 %574
  %577 = load i32, i32* %47, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %576, i32 %577)
  %578 = load i32, i32* %47, align 4
  %579 = sext i32 %578 to i64
  %580 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %581 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %580, i64 %579
  %582 = load i32, i32* %47, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %581, i32 %582)
  %583 = load i32, i32* %47, align 4
  %584 = sext i32 %583 to i64
  %585 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %586 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %585, i64 %584
  %587 = load i32, i32* %47, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %586, i32 %587)
  %588 = load i32, i32* %47, align 4
  %589 = sext i32 %588 to i64
  %590 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %591 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %590, i64 %589
  %592 = load i32, i32* %47, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %591, i32 %592)
  %593 = load i32, i32* %47, align 4
  %594 = or i32 %593, 16
  %595 = add nsw i32 %594, 256
  %596 = load i32, i32* %36, align 4
  %597 = srem i32 %595, %596
  store i32 %597, i32* %48, align 4
  %598 = load i32, i32* %48, align 4
  %599 = sext i32 %598 to i64
  %600 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %601 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %600, i64 %599
  %602 = load i32, i32* %47, align 4
  %603 = sext i32 %602 to i64
  %604 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %605 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %604, i64 %603
  store [3 x %struct.QLA_F_Complex]* %601, [3 x %struct.QLA_F_Complex]** %605, align 8
  %606 = load i32, i32* %48, align 4
  %607 = sext i32 %606 to i64
  %608 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %609 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %608, i64 %607
  %610 = load i32, i32* %47, align 4
  %611 = sext i32 %610 to i64
  %612 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %613 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %612, i64 %611
  store [3 x [4 x %struct.QLA_F_Complex]]* %609, [3 x [4 x %struct.QLA_F_Complex]]** %613, align 8
  %614 = load i32, i32* %48, align 4
  %615 = sext i32 %614 to i64
  %616 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %617 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %616, i64 %615
  %618 = load i32, i32* %47, align 4
  %619 = sext i32 %618 to i64
  %620 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %621 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %620, i64 %619
  store [3 x [3 x %struct.QLA_F_Complex]]* %617, [3 x [3 x %struct.QLA_F_Complex]]** %621, align 8
  br label %622

; <label>:622:                                    ; preds = %547
  %623 = load i32, i32* %47, align 4
  %624 = add nsw i32 %623, 1
  store i32 %624, i32* %47, align 4
  br label %543

; <label>:625:                                    ; preds = %543
  store double 3.600000e+02, double* %30, align 8
  store double 1.680000e+02, double* %29, align 8
  %626 = load double, double* %37, align 8
  %627 = load double, double* %29, align 8
  %628 = load double, double* %30, align 8
  %629 = fadd double %627, %628
  %630 = fdiv double %626, %629
  %631 = fadd double 1.000000e+00, %630
  %632 = fptosi double %631 to i32
  store i32 %632, i32* %34, align 4
  %633 = call double @dtime()
  store double %633, double* %31, align 8
  store i32 0, i32* %49, align 4
  br label %634

; <label>:634:                                    ; preds = %643, %625
  %635 = load i32, i32* %49, align 4
  %636 = load i32, i32* %34, align 4
  %637 = icmp slt i32 %635, %636
  br i1 %637, label %638, label %646

; <label>:638:                                    ; preds = %634
  %639 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %640 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %641 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %642 = load i32, i32* %36, align 4
  call void @QLA_F3_D_vpeq_spproj_M_times_pD([3 x [4 x %struct.QLA_F_Complex]]* %639, [3 x [3 x %struct.QLA_F_Complex]]* %640, [3 x [4 x %struct.QLA_F_Complex]]** %641, i32 0, i32 1, i32 %642)
  br label %643

; <label>:643:                                    ; preds = %638
  %644 = load i32, i32* %49, align 4
  %645 = add nsw i32 %644, 1
  store i32 %645, i32* %49, align 4
  br label %634

; <label>:646:                                    ; preds = %634
  %647 = call double @dtime()
  %648 = load double, double* %31, align 8
  %649 = fsub double %647, %648
  store double %649, double* %31, align 8
  %650 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %651 = load i32, i32* %36, align 4
  %652 = call float @sum_D([3 x [4 x %struct.QLA_F_Complex]]* %650, i32 %651)
  store float %652, float* %6, align 4
  %653 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.8, i32 0, i32 0))
  %654 = load float, float* %6, align 4
  %655 = fpext float %654 to double
  %656 = load double, double* %31, align 8
  %657 = load double, double* %30, align 8
  %658 = load i32, i32* %36, align 4
  %659 = sitofp i32 %658 to double
  %660 = fmul double %657, %659
  %661 = load i32, i32* %34, align 4
  %662 = sitofp i32 %661 to double
  %663 = fmul double %660, %662
  %664 = load double, double* %31, align 8
  %665 = fmul double 1.000000e+06, %664
  %666 = fdiv double %663, %665
  %667 = load double, double* %29, align 8
  %668 = load i32, i32* %36, align 4
  %669 = sitofp i32 %668 to double
  %670 = fmul double %667, %669
  %671 = load i32, i32* %34, align 4
  %672 = sitofp i32 %671 to double
  %673 = fmul double %670, %672
  %674 = load double, double* %31, align 8
  %675 = fmul double 1.000000e+06, %674
  %676 = fdiv double %673, %675
  %677 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %655, double %656, double %666, double %676)
  store i32 0, i32* %50, align 4
  br label %678

; <label>:678:                                    ; preds = %757, %646
  %679 = load i32, i32* %50, align 4
  %680 = load i32, i32* %36, align 4
  %681 = icmp slt i32 %679, %680
  br i1 %681, label %682, label %760

; <label>:682:                                    ; preds = %678
  %683 = load i32, i32* %50, align 4
  %684 = sext i32 %683 to i64
  %685 = load float*, float** %7, align 8
  %686 = getelementptr inbounds float, float* %685, i64 %684
  %687 = load i32, i32* %50, align 4
  call void @set_R(float* %686, i32 %687)
  %688 = load i32, i32* %50, align 4
  %689 = sext i32 %688 to i64
  %690 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %691 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %690, i64 %689
  %692 = load i32, i32* %50, align 4
  call void @set_C(%struct.QLA_F_Complex* %691, i32 %692)
  %693 = load i32, i32* %50, align 4
  %694 = sext i32 %693 to i64
  %695 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %696 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %695, i64 %694
  %697 = load i32, i32* %50, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %696, i32 %697)
  %698 = load i32, i32* %50, align 4
  %699 = sext i32 %698 to i64
  %700 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %701 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %700, i64 %699
  %702 = load i32, i32* %50, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %701, i32 %702)
  %703 = load i32, i32* %50, align 4
  %704 = sext i32 %703 to i64
  %705 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %706 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %705, i64 %704
  %707 = load i32, i32* %50, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %706, i32 %707)
  %708 = load i32, i32* %50, align 4
  %709 = sext i32 %708 to i64
  %710 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %711 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %710, i64 %709
  %712 = load i32, i32* %50, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %711, i32 %712)
  %713 = load i32, i32* %50, align 4
  %714 = sext i32 %713 to i64
  %715 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %716 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %715, i64 %714
  %717 = load i32, i32* %50, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %716, i32 %717)
  %718 = load i32, i32* %50, align 4
  %719 = sext i32 %718 to i64
  %720 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %721 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %720, i64 %719
  %722 = load i32, i32* %50, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %721, i32 %722)
  %723 = load i32, i32* %50, align 4
  %724 = sext i32 %723 to i64
  %725 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %726 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %725, i64 %724
  %727 = load i32, i32* %50, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %726, i32 %727)
  %728 = load i32, i32* %50, align 4
  %729 = or i32 %728, 16
  %730 = add nsw i32 %729, 256
  %731 = load i32, i32* %36, align 4
  %732 = srem i32 %730, %731
  store i32 %732, i32* %51, align 4
  %733 = load i32, i32* %51, align 4
  %734 = sext i32 %733 to i64
  %735 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %736 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %735, i64 %734
  %737 = load i32, i32* %50, align 4
  %738 = sext i32 %737 to i64
  %739 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %740 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %739, i64 %738
  store [3 x %struct.QLA_F_Complex]* %736, [3 x %struct.QLA_F_Complex]** %740, align 8
  %741 = load i32, i32* %51, align 4
  %742 = sext i32 %741 to i64
  %743 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %744 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %743, i64 %742
  %745 = load i32, i32* %50, align 4
  %746 = sext i32 %745 to i64
  %747 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %748 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %747, i64 %746
  store [3 x [4 x %struct.QLA_F_Complex]]* %744, [3 x [4 x %struct.QLA_F_Complex]]** %748, align 8
  %749 = load i32, i32* %51, align 4
  %750 = sext i32 %749 to i64
  %751 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %752 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %751, i64 %750
  %753 = load i32, i32* %50, align 4
  %754 = sext i32 %753 to i64
  %755 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %756 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %755, i64 %754
  store [3 x [3 x %struct.QLA_F_Complex]]* %752, [3 x [3 x %struct.QLA_F_Complex]]** %756, align 8
  br label %757

; <label>:757:                                    ; preds = %682
  %758 = load i32, i32* %50, align 4
  %759 = add nsw i32 %758, 1
  store i32 %759, i32* %50, align 4
  br label %678

; <label>:760:                                    ; preds = %678
  store double 2.160000e+02, double* %30, align 8
  store double 1.980000e+02, double* %29, align 8
  %761 = load double, double* %37, align 8
  %762 = load double, double* %29, align 8
  %763 = load double, double* %30, align 8
  %764 = fadd double %762, %763
  %765 = fdiv double %761, %764
  %766 = fadd double 1.000000e+00, %765
  %767 = fptosi double %766 to i32
  store i32 %767, i32* %34, align 4
  %768 = call double @dtime()
  store double %768, double* %31, align 8
  store i32 0, i32* %52, align 4
  br label %769

; <label>:769:                                    ; preds = %778, %760
  %770 = load i32, i32* %52, align 4
  %771 = load i32, i32* %34, align 4
  %772 = icmp slt i32 %770, %771
  br i1 %772, label %773, label %781

; <label>:773:                                    ; preds = %769
  %774 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %775 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %776 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %777 = load i32, i32* %36, align 4
  call void @QLA_F3_M_veq_M_times_pM([3 x [3 x %struct.QLA_F_Complex]]* %774, [3 x [3 x %struct.QLA_F_Complex]]* %775, [3 x [3 x %struct.QLA_F_Complex]]** %776, i32 %777)
  br label %778

; <label>:778:                                    ; preds = %773
  %779 = load i32, i32* %52, align 4
  %780 = add nsw i32 %779, 1
  store i32 %780, i32* %52, align 4
  br label %769

; <label>:781:                                    ; preds = %769
  %782 = call double @dtime()
  %783 = load double, double* %31, align 8
  %784 = fsub double %782, %783
  store double %784, double* %31, align 8
  %785 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %786 = load i32, i32* %36, align 4
  %787 = call float @sum_M([3 x [3 x %struct.QLA_F_Complex]]* %785, i32 %786)
  store float %787, float* %6, align 4
  %788 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.9, i32 0, i32 0))
  %789 = load float, float* %6, align 4
  %790 = fpext float %789 to double
  %791 = load double, double* %31, align 8
  %792 = load double, double* %30, align 8
  %793 = load i32, i32* %36, align 4
  %794 = sitofp i32 %793 to double
  %795 = fmul double %792, %794
  %796 = load i32, i32* %34, align 4
  %797 = sitofp i32 %796 to double
  %798 = fmul double %795, %797
  %799 = load double, double* %31, align 8
  %800 = fmul double 1.000000e+06, %799
  %801 = fdiv double %798, %800
  %802 = load double, double* %29, align 8
  %803 = load i32, i32* %36, align 4
  %804 = sitofp i32 %803 to double
  %805 = fmul double %802, %804
  %806 = load i32, i32* %34, align 4
  %807 = sitofp i32 %806 to double
  %808 = fmul double %805, %807
  %809 = load double, double* %31, align 8
  %810 = fmul double 1.000000e+06, %809
  %811 = fdiv double %808, %810
  %812 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %790, double %791, double %801, double %811)
  store i32 0, i32* %53, align 4
  br label %813

; <label>:813:                                    ; preds = %892, %781
  %814 = load i32, i32* %53, align 4
  %815 = load i32, i32* %36, align 4
  %816 = icmp slt i32 %814, %815
  br i1 %816, label %817, label %895

; <label>:817:                                    ; preds = %813
  %818 = load i32, i32* %53, align 4
  %819 = sext i32 %818 to i64
  %820 = load float*, float** %7, align 8
  %821 = getelementptr inbounds float, float* %820, i64 %819
  %822 = load i32, i32* %53, align 4
  call void @set_R(float* %821, i32 %822)
  %823 = load i32, i32* %53, align 4
  %824 = sext i32 %823 to i64
  %825 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %826 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %825, i64 %824
  %827 = load i32, i32* %53, align 4
  call void @set_C(%struct.QLA_F_Complex* %826, i32 %827)
  %828 = load i32, i32* %53, align 4
  %829 = sext i32 %828 to i64
  %830 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %831 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %830, i64 %829
  %832 = load i32, i32* %53, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %831, i32 %832)
  %833 = load i32, i32* %53, align 4
  %834 = sext i32 %833 to i64
  %835 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %836 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %835, i64 %834
  %837 = load i32, i32* %53, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %836, i32 %837)
  %838 = load i32, i32* %53, align 4
  %839 = sext i32 %838 to i64
  %840 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %841 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %840, i64 %839
  %842 = load i32, i32* %53, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %841, i32 %842)
  %843 = load i32, i32* %53, align 4
  %844 = sext i32 %843 to i64
  %845 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %846 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %845, i64 %844
  %847 = load i32, i32* %53, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %846, i32 %847)
  %848 = load i32, i32* %53, align 4
  %849 = sext i32 %848 to i64
  %850 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %851 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %850, i64 %849
  %852 = load i32, i32* %53, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %851, i32 %852)
  %853 = load i32, i32* %53, align 4
  %854 = sext i32 %853 to i64
  %855 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %856 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %855, i64 %854
  %857 = load i32, i32* %53, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %856, i32 %857)
  %858 = load i32, i32* %53, align 4
  %859 = sext i32 %858 to i64
  %860 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %861 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %860, i64 %859
  %862 = load i32, i32* %53, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %861, i32 %862)
  %863 = load i32, i32* %53, align 4
  %864 = or i32 %863, 16
  %865 = add nsw i32 %864, 256
  %866 = load i32, i32* %36, align 4
  %867 = srem i32 %865, %866
  store i32 %867, i32* %54, align 4
  %868 = load i32, i32* %54, align 4
  %869 = sext i32 %868 to i64
  %870 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %871 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %870, i64 %869
  %872 = load i32, i32* %53, align 4
  %873 = sext i32 %872 to i64
  %874 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %875 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %874, i64 %873
  store [3 x %struct.QLA_F_Complex]* %871, [3 x %struct.QLA_F_Complex]** %875, align 8
  %876 = load i32, i32* %54, align 4
  %877 = sext i32 %876 to i64
  %878 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %879 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %878, i64 %877
  %880 = load i32, i32* %53, align 4
  %881 = sext i32 %880 to i64
  %882 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %883 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %882, i64 %881
  store [3 x [4 x %struct.QLA_F_Complex]]* %879, [3 x [4 x %struct.QLA_F_Complex]]** %883, align 8
  %884 = load i32, i32* %54, align 4
  %885 = sext i32 %884 to i64
  %886 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %887 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %886, i64 %885
  %888 = load i32, i32* %53, align 4
  %889 = sext i32 %888 to i64
  %890 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %891 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %890, i64 %889
  store [3 x [3 x %struct.QLA_F_Complex]]* %887, [3 x [3 x %struct.QLA_F_Complex]]** %891, align 8
  br label %892

; <label>:892:                                    ; preds = %817
  %893 = load i32, i32* %53, align 4
  %894 = add nsw i32 %893, 1
  store i32 %894, i32* %53, align 4
  br label %813

; <label>:895:                                    ; preds = %813
  store double 2.400000e+01, double* %30, align 8
  store double 1.200000e+01, double* %29, align 8
  %896 = load double, double* %37, align 8
  %897 = load double, double* %29, align 8
  %898 = load double, double* %30, align 8
  %899 = fadd double %897, %898
  %900 = fdiv double %896, %899
  %901 = fadd double 1.000000e+00, %900
  %902 = fptosi double %901 to i32
  store i32 %902, i32* %34, align 4
  %903 = call double @dtime()
  store double %903, double* %31, align 8
  store i32 0, i32* %55, align 4
  br label %904

; <label>:904:                                    ; preds = %912, %895
  %905 = load i32, i32* %55, align 4
  %906 = load i32, i32* %34, align 4
  %907 = icmp slt i32 %905, %906
  br i1 %907, label %908, label %915

; <label>:908:                                    ; preds = %904
  %909 = load float*, float** %7, align 8
  %910 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %911 = load i32, i32* %36, align 4
  call void @QLA_F3_r_veq_norm2_V(float* %909, [3 x %struct.QLA_F_Complex]* %910, i32 %911)
  br label %912

; <label>:912:                                    ; preds = %908
  %913 = load i32, i32* %55, align 4
  %914 = add nsw i32 %913, 1
  store i32 %914, i32* %55, align 4
  br label %904

; <label>:915:                                    ; preds = %904
  %916 = call double @dtime()
  %917 = load double, double* %31, align 8
  %918 = fsub double %916, %917
  store double %918, double* %31, align 8
  %919 = load float*, float** %7, align 8
  %920 = load float, float* %919, align 4
  store float %920, float* %6, align 4
  %921 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.10, i32 0, i32 0))
  %922 = load float, float* %6, align 4
  %923 = fpext float %922 to double
  %924 = load double, double* %31, align 8
  %925 = load double, double* %30, align 8
  %926 = load i32, i32* %36, align 4
  %927 = sitofp i32 %926 to double
  %928 = fmul double %925, %927
  %929 = load i32, i32* %34, align 4
  %930 = sitofp i32 %929 to double
  %931 = fmul double %928, %930
  %932 = load double, double* %31, align 8
  %933 = fmul double 1.000000e+06, %932
  %934 = fdiv double %931, %933
  %935 = load double, double* %29, align 8
  %936 = load i32, i32* %36, align 4
  %937 = sitofp i32 %936 to double
  %938 = fmul double %935, %937
  %939 = load i32, i32* %34, align 4
  %940 = sitofp i32 %939 to double
  %941 = fmul double %938, %940
  %942 = load double, double* %31, align 8
  %943 = fmul double 1.000000e+06, %942
  %944 = fdiv double %941, %943
  %945 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %923, double %924, double %934, double %944)
  store i32 0, i32* %56, align 4
  br label %946

; <label>:946:                                    ; preds = %1025, %915
  %947 = load i32, i32* %56, align 4
  %948 = load i32, i32* %36, align 4
  %949 = icmp slt i32 %947, %948
  br i1 %949, label %950, label %1028

; <label>:950:                                    ; preds = %946
  %951 = load i32, i32* %56, align 4
  %952 = sext i32 %951 to i64
  %953 = load float*, float** %7, align 8
  %954 = getelementptr inbounds float, float* %953, i64 %952
  %955 = load i32, i32* %56, align 4
  call void @set_R(float* %954, i32 %955)
  %956 = load i32, i32* %56, align 4
  %957 = sext i32 %956 to i64
  %958 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %959 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %958, i64 %957
  %960 = load i32, i32* %56, align 4
  call void @set_C(%struct.QLA_F_Complex* %959, i32 %960)
  %961 = load i32, i32* %56, align 4
  %962 = sext i32 %961 to i64
  %963 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %964 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %963, i64 %962
  %965 = load i32, i32* %56, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %964, i32 %965)
  %966 = load i32, i32* %56, align 4
  %967 = sext i32 %966 to i64
  %968 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %969 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %968, i64 %967
  %970 = load i32, i32* %56, align 4
  call void @set_V([3 x %struct.QLA_F_Complex]* %969, i32 %970)
  %971 = load i32, i32* %56, align 4
  %972 = sext i32 %971 to i64
  %973 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %21, align 8
  %974 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %973, i64 %972
  %975 = load i32, i32* %56, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %974, i32 %975)
  %976 = load i32, i32* %56, align 4
  %977 = sext i32 %976 to i64
  %978 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %979 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %978, i64 %977
  %980 = load i32, i32* %56, align 4
  call void @set_D([3 x [4 x %struct.QLA_F_Complex]]* %979, i32 %980)
  %981 = load i32, i32* %56, align 4
  %982 = sext i32 %981 to i64
  %983 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %24, align 8
  %984 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %983, i64 %982
  %985 = load i32, i32* %56, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %984, i32 %985)
  %986 = load i32, i32* %56, align 4
  %987 = sext i32 %986 to i64
  %988 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %25, align 8
  %989 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %988, i64 %987
  %990 = load i32, i32* %56, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %989, i32 %990)
  %991 = load i32, i32* %56, align 4
  %992 = sext i32 %991 to i64
  %993 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %994 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %993, i64 %992
  %995 = load i32, i32* %56, align 4
  call void @set_M([3 x [3 x %struct.QLA_F_Complex]]* %994, i32 %995)
  %996 = load i32, i32* %56, align 4
  %997 = or i32 %996, 16
  %998 = add nsw i32 %997, 256
  %999 = load i32, i32* %36, align 4
  %1000 = srem i32 %998, %999
  store i32 %1000, i32* %57, align 4
  %1001 = load i32, i32* %57, align 4
  %1002 = sext i32 %1001 to i64
  %1003 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %1004 = getelementptr inbounds [3 x %struct.QLA_F_Complex], [3 x %struct.QLA_F_Complex]* %1003, i64 %1002
  %1005 = load i32, i32* %56, align 4
  %1006 = sext i32 %1005 to i64
  %1007 = load [3 x %struct.QLA_F_Complex]**, [3 x %struct.QLA_F_Complex]*** %14, align 8
  %1008 = getelementptr inbounds [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %1007, i64 %1006
  store [3 x %struct.QLA_F_Complex]* %1004, [3 x %struct.QLA_F_Complex]** %1008, align 8
  %1009 = load i32, i32* %57, align 4
  %1010 = sext i32 %1009 to i64
  %1011 = load [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %22, align 8
  %1012 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]], [3 x [4 x %struct.QLA_F_Complex]]* %1011, i64 %1010
  %1013 = load i32, i32* %56, align 4
  %1014 = sext i32 %1013 to i64
  %1015 = load [3 x [4 x %struct.QLA_F_Complex]]**, [3 x [4 x %struct.QLA_F_Complex]]*** %23, align 8
  %1016 = getelementptr inbounds [3 x [4 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]** %1015, i64 %1014
  store [3 x [4 x %struct.QLA_F_Complex]]* %1012, [3 x [4 x %struct.QLA_F_Complex]]** %1016, align 8
  %1017 = load i32, i32* %57, align 4
  %1018 = sext i32 %1017 to i64
  %1019 = load [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %26, align 8
  %1020 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]], [3 x [3 x %struct.QLA_F_Complex]]* %1019, i64 %1018
  %1021 = load i32, i32* %56, align 4
  %1022 = sext i32 %1021 to i64
  %1023 = load [3 x [3 x %struct.QLA_F_Complex]]**, [3 x [3 x %struct.QLA_F_Complex]]*** %28, align 8
  %1024 = getelementptr inbounds [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]** %1023, i64 %1022
  store [3 x [3 x %struct.QLA_F_Complex]]* %1020, [3 x [3 x %struct.QLA_F_Complex]]** %1024, align 8
  br label %1025

; <label>:1025:                                   ; preds = %950
  %1026 = load i32, i32* %56, align 4
  %1027 = add nsw i32 %1026, 1
  store i32 %1027, i32* %56, align 4
  br label %946

; <label>:1028:                                   ; preds = %946
  store double 4.800000e+01, double* %30, align 8
  store double 2.400000e+01, double* %29, align 8
  %1029 = load double, double* %37, align 8
  %1030 = load double, double* %29, align 8
  %1031 = load double, double* %30, align 8
  %1032 = fadd double %1030, %1031
  %1033 = fdiv double %1029, %1032
  %1034 = fadd double 1.000000e+00, %1033
  %1035 = fptosi double %1034 to i32
  store i32 %1035, i32* %34, align 4
  %1036 = call double @dtime()
  store double %1036, double* %31, align 8
  store i32 0, i32* %58, align 4
  br label %1037

; <label>:1037:                                   ; preds = %1046, %1028
  %1038 = load i32, i32* %58, align 4
  %1039 = load i32, i32* %34, align 4
  %1040 = icmp slt i32 %1038, %1039
  br i1 %1040, label %1041, label %1049

; <label>:1041:                                   ; preds = %1037
  %1042 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %1043 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %9, align 8
  %1044 = load [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]** %10, align 8
  %1045 = load i32, i32* %36, align 4
  call void @QLA_F3_c_veq_V_dot_V(%struct.QLA_F_Complex* %1042, [3 x %struct.QLA_F_Complex]* %1043, [3 x %struct.QLA_F_Complex]* %1044, i32 %1045)
  br label %1046

; <label>:1046:                                   ; preds = %1041
  %1047 = load i32, i32* %58, align 4
  %1048 = add nsw i32 %1047, 1
  store i32 %1048, i32* %58, align 4
  br label %1037

; <label>:1049:                                   ; preds = %1037
  %1050 = call double @dtime()
  %1051 = load double, double* %31, align 8
  %1052 = fsub double %1050, %1051
  store double %1052, double* %31, align 8
  %1053 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %1054 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %1053, i32 0, i32 0
  %1055 = load float, float* %1054, align 8
  %1056 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %1057 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %1056, i32 0, i32 0
  %1058 = load float, float* %1057, align 8
  %1059 = fmul float %1055, %1058
  %1060 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %1061 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %1060, i32 0, i32 1
  %1062 = load float, float* %1061, align 4
  %1063 = load %struct.QLA_F_Complex*, %struct.QLA_F_Complex** %8, align 8
  %1064 = getelementptr inbounds %struct.QLA_F_Complex, %struct.QLA_F_Complex* %1063, i32 0, i32 1
  %1065 = load float, float* %1064, align 4
  %1066 = fmul float %1062, %1065
  %1067 = fadd float %1059, %1066
  store float %1067, float* %6, align 4
  %1068 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0))
  %1069 = load float, float* %6, align 4
  %1070 = fpext float %1069 to double
  %1071 = load double, double* %31, align 8
  %1072 = load double, double* %30, align 8
  %1073 = load i32, i32* %36, align 4
  %1074 = sitofp i32 %1073 to double
  %1075 = fmul double %1072, %1074
  %1076 = load i32, i32* %34, align 4
  %1077 = sitofp i32 %1076 to double
  %1078 = fmul double %1075, %1077
  %1079 = load double, double* %31, align 8
  %1080 = fmul double 1.000000e+06, %1079
  %1081 = fdiv double %1078, %1080
  %1082 = load double, double* %29, align 8
  %1083 = load i32, i32* %36, align 4
  %1084 = sitofp i32 %1083 to double
  %1085 = fmul double %1082, %1084
  %1086 = load i32, i32* %34, align 4
  %1087 = sitofp i32 %1086 to double
  %1088 = fmul double %1085, %1087
  %1089 = load double, double* %31, align 8
  %1090 = fmul double 1.000000e+06, %1089
  %1091 = fdiv double %1088, %1090
  %1092 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0), double %1070, double %1071, double %1081, double %1091)
  br label %1093

; <label>:1093:                                   ; preds = %1049
  %1094 = load i32, i32* %36, align 4
  %1095 = mul nsw i32 %1094, 2
  store i32 %1095, i32* %36, align 4
  br label %125

; <label>:1096:                                   ; preds = %125
  ret i32 0
}

declare i32 @printf(i8*, ...) #2

declare void @QLA_F3_V_vpeq_M_times_pV([3 x %struct.QLA_F_Complex]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x %struct.QLA_F_Complex]**, i32) #2

declare void @QLA_F3_V_veq_Ma_times_V([3 x %struct.QLA_F_Complex]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x %struct.QLA_F_Complex]*, i32) #2

declare void @QLA_F3_V_vmeq_pV([3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]**, i32) #2

declare void @QLA_F3_D_vpeq_spproj_M_times_pD([3 x [4 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [4 x %struct.QLA_F_Complex]]**, i32, i32, i32) #2

declare void @QLA_F3_M_veq_M_times_pM([3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]*, [3 x [3 x %struct.QLA_F_Complex]]**, i32) #2

declare void @QLA_F3_r_veq_norm2_V(float*, [3 x %struct.QLA_F_Complex]*, i32) #2

declare void @QLA_F3_c_veq_V_dot_V(%struct.QLA_F_Complex*, [3 x %struct.QLA_F_Complex]*, [3 x %struct.QLA_F_Complex]*, i32) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-svn288847-1~exp1 (branches/release_39)"}
