#include <omp.h>
int f() {return omp_get_num_threads();}
int g() {return f()+1;}
int main() {return g()+1;}
