; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@main.xx = internal global [15000 x float] zeroinitializer, align 16
@main.yy = internal global [15000 x float] zeroinitializer, align 16
@main.zz = internal global [15000 x float] zeroinitializer, align 16
@main.mass = internal global [15000 x float] zeroinitializer, align 16
@main.vx1 = internal global [15000 x float] zeroinitializer, align 16
@main.vy1 = internal global [15000 x float] zeroinitializer, align 16
@main.vz1 = internal global [15000 x float] zeroinitializer, align 16
@.str = private unnamed_addr constant [17 x i8] c"count is set %d\0A\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"Total MPI ranks %d\0A\00", align 1
@.str.2 = private unnamed_addr constant [27 x i8] c"Number of OMP threads %d\0A\0A\00", align 1
@.str.3 = private unnamed_addr constant [34 x i8] c"\0AKernel elapsed time, s: %18.8lf\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca float, align 4
  %7 = alloca float, align 4
  %8 = alloca float, align 4
  %9 = alloca float, align 4
  %10 = alloca float, align 4
  %11 = alloca float, align 4
  %12 = alloca [16777216 x i8], align 16
  %13 = alloca [16777216 x i8], align 16
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i64, align 8
  %20 = alloca i64, align 8
  %21 = alloca i64, align 8
  %22 = alloca i64, align 8
  %23 = alloca i64, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  %28 = alloca double, align 8
  %29 = alloca double, align 8
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  store i64 0, i64* %23, align 8
  store double 0.000000e+00, double* %27, align 8
  store i32 0, i32* %17, align 4
  store i32 1, i32* %18, align 4
  store i32 327, i32* %15, align 4
  %30 = load i32, i32* %17, align 4
  %31 = icmp eq i32 %30, 0
  br i1 %31, label %32, label %37

; <label>:32:                                     ; preds = %2
  %33 = load i32, i32* %15, align 4
  %34 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0), i32 %33)
  %35 = load i32, i32* %18, align 4
  %36 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0), i32 %35)
  br label %37

; <label>:37:                                     ; preds = %32, %2
  %38 = load i32, i32* %17, align 4
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %40, label %46

; <label>:40:                                     ; preds = %37
  %41 = call i32 @omp_get_thread_num()
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %43, label %46

; <label>:43:                                     ; preds = %40
  %44 = call i32 @omp_get_num_threads()
  %45 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0), i32 %44)
  br label %46

; <label>:46:                                     ; preds = %43, %40, %37
  store double 0.000000e+00, double* %29, align 8
  store i32 400, i32* %14, align 4
  br label %47

; <label>:47:                                     ; preds = %249, %46
  %48 = load i32, i32* %14, align 4
  %49 = icmp slt i32 %48, 15000
  br i1 %49, label %50, label %252

; <label>:50:                                     ; preds = %47
  store float 0x3FCD70A3E0000000, float* %8, align 4
  store float 5.000000e-01, float* %6, align 4
  store float 0x3F9EB851E0000000, float* %7, align 4
  %51 = load i32, i32* %14, align 4
  %52 = sitofp i32 %51 to float
  %53 = fdiv float 1.000000e+00, %52
  store float %53, float* %9, align 4
  %54 = load i32, i32* %14, align 4
  %55 = sitofp i32 %54 to float
  %56 = fdiv float 2.000000e+00, %55
  store float %56, float* %10, align 4
  %57 = load i32, i32* %14, align 4
  %58 = sitofp i32 %57 to float
  %59 = fdiv float 3.000000e+00, %58
  store float %59, float* %11, align 4
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.xx, i64 0, i64 0), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.yy, i64 0, i64 0), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.zz, i64 0, i64 0), align 16
  store float 2.000000e+00, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.mass, i64 0, i64 0), align 16
  store i32 1, i32* %16, align 4
  br label %60

; <label>:60:                                     ; preds = %106, %50
  %61 = load i32, i32* %16, align 4
  %62 = load i32, i32* %14, align 4
  %63 = icmp slt i32 %61, %62
  br i1 %63, label %64, label %109

; <label>:64:                                     ; preds = %60
  %65 = load i32, i32* %16, align 4
  %66 = sub nsw i32 %65, 1
  %67 = sext i32 %66 to i64
  %68 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %67
  %69 = load float, float* %68, align 4
  %70 = load float, float* %9, align 4
  %71 = fadd float %69, %70
  %72 = load i32, i32* %16, align 4
  %73 = sext i32 %72 to i64
  %74 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %73
  store float %71, float* %74, align 4
  %75 = load i32, i32* %16, align 4
  %76 = sub nsw i32 %75, 1
  %77 = sext i32 %76 to i64
  %78 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %77
  %79 = load float, float* %78, align 4
  %80 = load float, float* %10, align 4
  %81 = fadd float %79, %80
  %82 = load i32, i32* %16, align 4
  %83 = sext i32 %82 to i64
  %84 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %83
  store float %81, float* %84, align 4
  %85 = load i32, i32* %16, align 4
  %86 = sub nsw i32 %85, 1
  %87 = sext i32 %86 to i64
  %88 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %87
  %89 = load float, float* %88, align 4
  %90 = load float, float* %11, align 4
  %91 = fadd float %89, %90
  %92 = load i32, i32* %16, align 4
  %93 = sext i32 %92 to i64
  %94 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %93
  store float %91, float* %94, align 4
  %95 = load i32, i32* %16, align 4
  %96 = sitofp i32 %95 to float
  %97 = fmul float %96, 0x3F847AE140000000
  %98 = load i32, i32* %16, align 4
  %99 = sext i32 %98 to i64
  %100 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %99
  %101 = load float, float* %100, align 4
  %102 = fadd float %97, %101
  %103 = load i32, i32* %16, align 4
  %104 = sext i32 %103 to i64
  %105 = getelementptr inbounds [15000 x float], [15000 x float]* @main.mass, i64 0, i64 %104
  store float %102, float* %105, align 4
  br label %106

; <label>:106:                                    ; preds = %64
  %107 = load i32, i32* %16, align 4
  %108 = add nsw i32 %107, 1
  store i32 %108, i32* %16, align 4
  br label %60

; <label>:109:                                    ; preds = %60
  store i32 0, i32* %16, align 4
  br label %110

; <label>:110:                                    ; preds = %124, %109
  %111 = load i32, i32* %16, align 4
  %112 = load i32, i32* %14, align 4
  %113 = icmp slt i32 %111, %112
  br i1 %113, label %114, label %127

; <label>:114:                                    ; preds = %110
  %115 = load i32, i32* %16, align 4
  %116 = sext i32 %115 to i64
  %117 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %116
  store float 0.000000e+00, float* %117, align 4
  %118 = load i32, i32* %16, align 4
  %119 = sext i32 %118 to i64
  %120 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %119
  store float 0.000000e+00, float* %120, align 4
  %121 = load i32, i32* %16, align 4
  %122 = sext i32 %121 to i64
  %123 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %122
  store float 0.000000e+00, float* %123, align 4
  br label %124

; <label>:124:                                    ; preds = %114
  %125 = load i32, i32* %16, align 4
  %126 = add nsw i32 %125, 1
  store i32 %126, i32* %16, align 4
  br label %110

; <label>:127:                                    ; preds = %110
  store i32 0, i32* %16, align 4
  br label %128

; <label>:128:                                    ; preds = %135, %127
  %129 = load i32, i32* %16, align 4
  %130 = icmp slt i32 %129, 16777216
  br i1 %130, label %131, label %138

; <label>:131:                                    ; preds = %128
  %132 = load i32, i32* %16, align 4
  %133 = sext i32 %132 to i64
  %134 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %12, i64 0, i64 %133
  store i8 4, i8* %134, align 1
  br label %135

; <label>:135:                                    ; preds = %131
  %136 = load i32, i32* %16, align 4
  %137 = add nsw i32 %136, 1
  store i32 %137, i32* %16, align 4
  br label %128

; <label>:138:                                    ; preds = %128
  store i32 0, i32* %16, align 4
  br label %139

; <label>:139:                                    ; preds = %150, %138
  %140 = load i32, i32* %16, align 4
  %141 = icmp slt i32 %140, 16777216
  br i1 %141, label %142, label %153

; <label>:142:                                    ; preds = %139
  %143 = load i32, i32* %16, align 4
  %144 = sext i32 %143 to i64
  %145 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %12, i64 0, i64 %144
  %146 = load i8, i8* %145, align 1
  %147 = load i32, i32* %16, align 4
  %148 = sext i32 %147 to i64
  %149 = getelementptr inbounds [16777216 x i8], [16777216 x i8]* %13, i64 0, i64 %148
  store i8 %146, i8* %149, align 1
  br label %150

; <label>:150:                                    ; preds = %142
  %151 = load i32, i32* %16, align 4
  %152 = add nsw i32 %151, 1
  store i32 %152, i32* %16, align 4
  br label %139

; <label>:153:                                    ; preds = %139
  %154 = call double (...) @mysecond()
  store double %154, double* %24, align 8
  store i32 0, i32* %16, align 4
  br label %155

; <label>:155:                                    ; preds = %208, %153
  %156 = load i32, i32* %16, align 4
  %157 = load i32, i32* %15, align 4
  %158 = icmp slt i32 %156, %157
  br i1 %158, label %159, label %211

; <label>:159:                                    ; preds = %155
  %160 = load i32, i32* %14, align 4
  %161 = load i32, i32* %16, align 4
  %162 = sext i32 %161 to i64
  %163 = getelementptr inbounds [15000 x float], [15000 x float]* @main.xx, i64 0, i64 %162
  %164 = load float, float* %163, align 4
  %165 = load i32, i32* %16, align 4
  %166 = sext i32 %165 to i64
  %167 = getelementptr inbounds [15000 x float], [15000 x float]* @main.yy, i64 0, i64 %166
  %168 = load float, float* %167, align 4
  %169 = load i32, i32* %16, align 4
  %170 = sext i32 %169 to i64
  %171 = getelementptr inbounds [15000 x float], [15000 x float]* @main.zz, i64 0, i64 %170
  %172 = load float, float* %171, align 4
  %173 = load float, float* %6, align 4
  %174 = load float, float* %7, align 4
  call void @Step10_orig(i32 %160, float %164, float %168, float %172, float %173, float %174, float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.xx, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.yy, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.zz, i32 0, i32 0), float* getelementptr inbounds ([15000 x float], [15000 x float]* @main.mass, i32 0, i32 0), float* %9, float* %10, float* %11)
  %175 = load i32, i32* %16, align 4
  %176 = sext i32 %175 to i64
  %177 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %176
  %178 = load float, float* %177, align 4
  %179 = load float, float* %9, align 4
  %180 = load float, float* %8, align 4
  %181 = fmul float %179, %180
  %182 = fadd float %178, %181
  %183 = load i32, i32* %16, align 4
  %184 = sext i32 %183 to i64
  %185 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %184
  store float %182, float* %185, align 4
  %186 = load i32, i32* %16, align 4
  %187 = sext i32 %186 to i64
  %188 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %187
  %189 = load float, float* %188, align 4
  %190 = load float, float* %10, align 4
  %191 = load float, float* %8, align 4
  %192 = fmul float %190, %191
  %193 = fadd float %189, %192
  %194 = load i32, i32* %16, align 4
  %195 = sext i32 %194 to i64
  %196 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %195
  store float %193, float* %196, align 4
  %197 = load i32, i32* %16, align 4
  %198 = sext i32 %197 to i64
  %199 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %198
  %200 = load float, float* %199, align 4
  %201 = load float, float* %11, align 4
  %202 = load float, float* %8, align 4
  %203 = fmul float %201, %202
  %204 = fadd float %200, %203
  %205 = load i32, i32* %16, align 4
  %206 = sext i32 %205 to i64
  %207 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %206
  store float %204, float* %207, align 4
  br label %208

; <label>:208:                                    ; preds = %159
  %209 = load i32, i32* %16, align 4
  %210 = add nsw i32 %209, 1
  store i32 %210, i32* %16, align 4
  br label %155

; <label>:211:                                    ; preds = %155
  %212 = call double (...) @mysecond()
  store double %212, double* %25, align 8
  store double 0.000000e+00, double* %28, align 8
  store i32 0, i32* %16, align 4
  br label %213

; <label>:213:                                    ; preds = %235, %211
  %214 = load i32, i32* %16, align 4
  %215 = load i32, i32* %14, align 4
  %216 = icmp slt i32 %214, %215
  br i1 %216, label %217, label %238

; <label>:217:                                    ; preds = %213
  %218 = load double, double* %28, align 8
  %219 = load i32, i32* %16, align 4
  %220 = sext i32 %219 to i64
  %221 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vx1, i64 0, i64 %220
  %222 = load float, float* %221, align 4
  %223 = load i32, i32* %16, align 4
  %224 = sext i32 %223 to i64
  %225 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vy1, i64 0, i64 %224
  %226 = load float, float* %225, align 4
  %227 = fadd float %222, %226
  %228 = load i32, i32* %16, align 4
  %229 = sext i32 %228 to i64
  %230 = getelementptr inbounds [15000 x float], [15000 x float]* @main.vz1, i64 0, i64 %229
  %231 = load float, float* %230, align 4
  %232 = fadd float %227, %231
  %233 = fpext float %232 to double
  %234 = fadd double %218, %233
  store double %234, double* %28, align 8
  br label %235

; <label>:235:                                    ; preds = %217
  %236 = load i32, i32* %16, align 4
  %237 = add nsw i32 %236, 1
  store i32 %237, i32* %16, align 4
  br label %213

; <label>:238:                                    ; preds = %213
  %239 = load double, double* %29, align 8
  %240 = load double, double* %28, align 8
  %241 = fadd double %239, %240
  store double %241, double* %29, align 8
  %242 = load double, double* %25, align 8
  %243 = load double, double* %24, align 8
  %244 = fsub double %242, %243
  %245 = fmul double %244, 1.000000e+06
  store double %245, double* %26, align 8
  %246 = load double, double* %27, align 8
  %247 = load double, double* %26, align 8
  %248 = fadd double %246, %247
  store double %248, double* %27, align 8
  br label %249

; <label>:249:                                    ; preds = %238
  %250 = load i32, i32* %14, align 4
  %251 = add nsw i32 %250, 20
  store i32 %251, i32* %14, align 4
  br label %47

; <label>:252:                                    ; preds = %47
  %253 = load i32, i32* %17, align 4
  %254 = icmp eq i32 %253, 0
  br i1 %254, label %255, label %259

; <label>:255:                                    ; preds = %252
  %256 = load double, double* %27, align 8
  %257 = fmul double %256, 1.000000e-06
  %258 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.3, i32 0, i32 0), double %257)
  br label %259

; <label>:259:                                    ; preds = %255, %252
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @omp_get_thread_num() #1

declare i32 @omp_get_num_threads() #1

declare double @mysecond(...) #1

declare void @Step10_orig(i32, float, float, float, float, float, float*, float*, float*, float*, float*, float*, float*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.1-svn288847-1~exp1 (branches/release_39)"}
