#include "Analyzer.h"

#include "iostream"
#include "iomanip"

#include "llvm/IR/Function.h"

using namespace llvm;
using namespace std;


void analyzeKeyPerformanceIndicators(const Module *M)
{
  // Collect statistics
  unsigned nFunctions = 0;
  unsigned nInstructions = 0;
  for (const Function &F : *M) {
      nFunctions++;
      for (const BasicBlock &BB : F) {
        for (const Instruction &I : BB) {
            nInstructions++;
        }
      }
  }

  // Get benchmark name
  string bench = M->getName().str();

  // Dump statistics
  std::cout << "\n";
  std::cout << "  " << "Functions" << " " << "Instructions" << " " << "File" << "\n";
  std::cout << "  " << std::right << std::setw(8) << std::setfill(' ')<< nFunctions
            << "  " << std::right << std::setw(11) << std::setfill(' ')<< nInstructions
            << "  " << bench
            << "\n";
}

