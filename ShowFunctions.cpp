#include "ShowFunctions.h"

#include "iostream"
#include "iomanip"
#include "vector"
#include "string"
#include "algorithm"

#include "llvm/IR/Function.h"
#include "llvm/ADT/StringRef.h"

using namespace llvm;
using namespace std;

void listfunctions(const Module *M)
{
  std::vector<std::string> fNames;
  std::cout << "\n\n" << "LAB 31:" << "\n";
  // Obtenemos el nombre de las funciones y lo insertamos en el vector
  for (const Function &F : *M) {
      fNames.push_back(F.getName().str());
  }
  // Ordenamos el vector en orden creciente
  std::sort(fNames.begin(),fNames.end());
  // Sacamos el resultado por pantalla
  for (const string &it: fNames) {
      std::cout << it << "\n";
  }
}
