#include "ShowFunctionCalls.h"

#include "iostream"
#include "iomanip"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"

using namespace llvm;
using namespace std;

void listfunctioncalls(const Module *M)
{
  map<string,vector<string>> functionlist;
  for (const Function &F : *M) {
     vector<string> calllist;
     for (const BasicBlock &BB : F) {
        for (const Instruction &I : BB) {
           if (const CallInst *CI=dyn_cast<CallInst>(&I)) {
             calllist.push_back(CI->getCalledFunction()->getName().str());
           }
        }
     }
// Si queremos la lista de llamadas ordenada de forma creciente
// por nombre de funcion descomentamos la linea siguiente:
//     std::sort(calllist.begin(),calllist.end());
     functionlist[F.getName().str()]=calllist;
  }

// Mostrar listado por pantalla
  cout << "\n\n" << "LAB 32:" << "\n";
  if (!functionlist.empty()) {
    for (auto const &fname : functionlist) {
       cout << fname.first << "\n";
       for (auto const &cname : fname.second) {
          cout << " " << cname << "\n";
       }
    }
  }
}
