#include "ShowInterproceduralFunctions.h"

#include "iostream"
#include "iomanip"
#include "vector"
#include "string"

#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Analysis/CallGraph.h"

using namespace llvm;
using namespace std;

bool isanOpenMPcall(const string I)
{
if ((I.compare(0,4,"omp_")==0)||(I.compare(0,7,"__kmpc_")==0)) return true;
return false;
}

void walkNode(const llvm::CallGraphNode *GN,std::vector<std::string> &list)
{
  for (unsigned i=0;i<(GN->size());i++) {
     walkNode((*GN)[i],list);
  }
  const Function *funcion=GN->getFunction();
  if (funcion!=nullptr) {
    string functionName=GN->getFunction()->getName().str();
    if ((!functionName.empty()) and (isanOpenMPcall(functionName)))
      list.push_back(functionName);
  }
}

void walkALLnodes(const llvm::CallGraphNode *GN,std::map<string,std::vector<string>> &functionlist)
{
  for (unsigned i=0;i<(GN->size());i++) {
     walkALLnodes((*GN)[i],functionlist);
     
  }
  const Function *funcion=GN->getFunction();
  if (funcion!=nullptr) {
    string functionName=GN->getFunction()->getName().str();
    if ((!functionName.empty()) and (!isanOpenMPcall(functionName))) {
     vector<string> lista;
     walkNode(GN,lista);
     if (!lista.empty()) functionlist[functionName]=lista;
    }
  }
}

void listinterproceduralfunctions(const llvm::CallGraph *CG, const llvm::Module *M)
{

  cout << "\n\n" << "LAB 41:" << "\n";

  if (M->getFunction(StringRef("main"))!=nullptr) {
     cout << "main\n";
     std::vector<std::string> calllist;
     walkNode((*CG)[M->getFunction(StringRef("main"))],calllist);
     // ordenar de menor a mayor
     std::sort(calllist.begin(),calllist.end());
     // eliminar duplicados
     calllist.erase(std::unique(calllist.begin(),calllist.end()),calllist.end());
     for (const string &fName: calllist)
        cout << " " << fName << "\n";
     if (calllist.empty())
       cout << " No OpenMP Calls." << "\n";
    } else {
     cout << "No main function.\n";
    }

  cout << "\n\n" << "LAB 42:" << "\n";

  map<string,vector<string>> functionlist;
  walkALLnodes(CG->getExternalCallingNode(),functionlist);
// Mostrar listado por pantalla
  if (!functionlist.empty()) {
    for (auto &fname : functionlist) {
       cout << fname.first << "\n";
       // ordenar de menor a mayor
       std::sort(fname.second.begin(),fname.second.end());
       // eliminar duplicados
       fname.second.erase(std::unique(fname.second.begin(),fname.second.end()),fname.second.end());
       for (auto const &cname : fname.second) {
          cout << " " << cname << "\n";
       }
    }
  } else { cout << "No functions with OpenMP Calls." << "\n"; }
}
